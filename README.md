# SLIDESHOW GUIDE #

1. Add new gem to **Gemfile**. Commit: ffd3d2b
2. Run: bundle
3. Add slideshow require to **app/assets/stylesheets/application.css**. Commit: 161838d
4. Add javascript require to **app/assets/javascripts/application.js**. Commit: 9b3b8cb
5. Add the necessary html need to display the slideshow to **app/views/products/index.html.erb**. Commit: 4efe7c1
6. Add the jquery code to run the slideshow at **app/assets/javascripts/application.js**. Commit: 2f08abf

##To see another guide and all of the documentation relating to owlcarousel visit: https://github.com/acrogenesis/owlcarousel-rails##