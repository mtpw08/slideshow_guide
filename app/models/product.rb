# app\models\product.rb
class Product < ActiveRecord::Base

	validates :title, :presence => { :message => "cannot be blank ..."}
	validates :title, :length => {:in => 2..35, :message => "must be between two and thirty five characters"}
  
	
	validates :description, :length => {:maximum => 200, :message => "maximum length 2000 characters"} 
  
	validates :img_url, :allow_blank => true, :format=> {:with => %r{\.(gif|jpg|png)$}i,:multiline => true, :message => "must be a URL for GIF, JPG or PNG images."}
	
	validates :price, :numericality => {:greater_than_or_equal_to => 0, :allow_blank => true, :message => "must be a positive value"}

	# see Timeliness GEM
	# validates :date_published, :timeliness => {  :type => :date,
	#		  :allow_blank => true, :invalid_date_message => "Date Published: is not a valid date ..."}

	# convert date to display in UK format
	def date_published_format
		if self.date_published !=nil
		#	self.date_published = date_published.strftime("%d-%m-%Y") 
		end
	end

	# a simple search which returns all objects whose title 
	# is the same as the value of a search_string
	def self.simple_search(search_string)
		self.where("title = ?", search_string)
	end

	# a fuzzy search which returns all objects whose title 
	# contains some part of the value of a search_string
	def self.fuzzy_search(search_string)
		search_string = "%" + search_string + "%"
		# self.where("title 
	end

end
